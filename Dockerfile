# Build :                      docker build . -t mon-site-release:mon-tag
# Si on a oublié de tagger :   docker tag IMAGE_ID mon-site-release:alpha
# Start with :                 docker run --rm -p 8080:8080

FROM php:fpm-alpine
COPY index.php /var/www/html
CMD php -S 0.0.0.0:8080
